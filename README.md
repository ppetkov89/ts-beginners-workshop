## Installation Instructions

Clone this repo.

```sh
# Installs all dependencies
npm install

# Starts the first exercise
npm run exercise 01
```

## How to take the workshop

You'll notice that the course is split into exercises. Each exercise is split into a `*.problem.ts` and a `*.solution.ts`.

To take an exercise:

1. Go into `*.problem.ts`
2. Run `npm run exercise 01`, where `01` is the number of the exercise you're on.

The `exercise` script will run TypeScript typechecks and a test suite on the exercise.

This workshop encourages **active, exploratory learning**. To attempt a solution, you'll need to:

1. Check out [TypeScript's docs](https://www.typescriptlang.org/docs/handbook/intro.html)
2. Try to find something that looks relevant.
3. Give it a go to see if it solves the problem.

You'll know if you've succeeded because the tests will pass.

### `npm run exercise 01`

Alias: `npm run e 01`

Run the corresponding `*.problem.ts` file.