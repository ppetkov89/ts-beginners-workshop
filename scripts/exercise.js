const { execSync } = require("child_process");
const fs = require("fs");
const path = require("path");
const chokidar = require("chokidar");
const glob = require("glob");

const [, , exercise] = process.argv;

if (!exercise) {
  console.log("Please specify an exercise");
  process.exit(1);
}

let pathIndicator = "problem";

if (process.env.SOLUTION) {
  pathIndicator = "solution";
}

const exercisePath = glob.globSync(
	`**/src/${exercise}/*.${pathIndicator}*.ts`
)[0];

if (!exercisePath) {
  console.log(`Exercise ${exercise} not found`);
  process.exit(1);
}

const exerciseFile = path.resolve(exercisePath);

// One-liner for current directory
chokidar.watch(exerciseFile).on("all", (event, path) => {
  const fileContents = fs.readFileSync(exerciseFile, "utf8");

  const containsVitest = fileContents.includes("vitest");
  try {
    console.clear();
    if (containsVitest) {
      console.log("Running tests...");
      execSync(`vitest run "${exerciseFile}" --passWithNoTests`, {
        stdio: "inherit",
      });
    }
    console.log("Checking types...");
    execSync(`tsc "${exerciseFile}" --noEmit --strict`, {
      stdio: "inherit",
    });
    console.log("Typecheck complete. You finished the exercise!");
  } catch (e) {
    console.log("Failed. Try again!");
  }
  process.exit(0);
});
