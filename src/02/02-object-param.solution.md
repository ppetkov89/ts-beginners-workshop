# Add Types to Object Params

There are multiple solutions to this problem.

## Solution 1: Pass an Object Type Directly

Perhaps the simplest solution is to pass an object type directly to the params argument.

Use curly braces to represent the object, then create type inline:

```js
export const addTwoNumbers = (params: { first: number; second: number }) => {
```

Note that TypeScript will understand if you use a comma between the `first` and `second` types like this:

```js
params: {first: number, second: number}
```

However, Prettier will correct it to use semicolons instead.

## Solution 2: Create a Named Type

A useful technique is creating a named type. Use the `type` keyword, then provide a name and object similar to the previous solution.

In this case, we'll name the type `AddTowNumbersArgs` and type `first` and `second` as `number`:

```js
type AddTwoNumbersArgs = {
	first: number,
	second: number,
};
```

Now we can set the `params` to be typed as `AddTowNumbersArgs`:

```js
export const addTwoNumbers = (params: AddTwoNumbersArgs) => {
	return params.first + params.second;
};
```

## Solution 3: Create an Interface

Interfaces can be used to represent objects (they can do other things, but we're only concerned with objects for now).

Using `interface` is similar to using `type`.

```js
interface AddTwoNumbersArgs {
	first: number;
	second: number;
}

export const addTwoNumbers = (params: AddTwoNumbersArgs) => {
	return params.first + params.second;
};
```

## Choosing Inline vs. Type vs. Interface

The inline technique used in the first solution will provide a verbose error that includes the full object type:

```js
Argument of type 'number' is not assignable to a parameter of type'{first: number; second: number;}'
```

But generally speaking, you should be using aliases instead of going inline.

So when should you choose `type` and when should you choose `interface`?

It comes down to the syntax you prefer.

Technically, type can represent anything. It could be a number, or a string, or a boolean.

TypeScript will give you an error if you try to use a string with `interface`:

```js

// this won't work!

interface AddTwoNumbersArgs = string

```

When you're just getting started, it doesn't really matter if you choose `type` or `interface`.

Just be consistent!
