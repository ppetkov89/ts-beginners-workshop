# Working with Object Params

Here's a different implementation of the addTwoNumbers function:

```js
export const addTwoNumbers = (params) => {
	return params.first + params.second;
};
```

This time the function accepts a `params` object with `first` and `second` properties.

```js
{
  first: 2,
  second: 4,
}
```

Similarly to last time, we're getting the "implicitly has an 'any' type" error.

## Challenge

Work out how to type `params` as an object with a key of `first` that is a number and a key of `second` that is also a number.
Hint: there are a few ways to solve this challenge - see what you can come up with!
