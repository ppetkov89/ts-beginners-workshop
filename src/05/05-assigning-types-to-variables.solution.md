# Make a Variable Conform to an Interface

There's a little bit of syntax we need to add to our declaration to solve this problem.

By adding `: User` to `defaultUser`, we're telling TypeScript that we want it to conform to our User interface.

```js

const defaultUser: User = {}
```

Now TypeScript will display errors at the line where `defaultUser` is declared.

We also benefit from autocompletion for the properties!

The same `: Type` syntax can be used with other types, including numbers and functions:

```js

let a: number = 1
```

As you write TypeScript, you need to think about `where` you want your contracts to be, and `what` needs to be done in order to meet them.