# Denote an Optional Property

This is a one character solution.

Adding a `?` before the colon tells TypeScript that the property isn't required in order for the function to work.

With this update, we don't have to pass in a `last` if we don't want to:

```js
export const getName = (params: { first: string; last?: string }) => {
```

Adding `?` will also work when using named types, or anywhere else object keys can be specified.

```js
type Params = {
	first: string,
	last?: string,
};
```

Tooling in VS Code is smart enough to recognize if a key is optional. This can be seen in the autocomplete, where you will be reminded that `last` should be either a string or undefined.

If you have prior experience with TypeScript, you might have seen a type explicitly set to `string` or `undefined` as seen below:

```js
type Params = { first: string; last: string | undefined }
```

However, written this way we do have to pass `last`. More on "or" later.

If you want to set a property as optional, use the `?`.
