# Techniques for Typing Dynamic Object Keys

This is another challenge with multiple solutions.

## Use the Record Utility Type

One solution is to type `cache` as a `Record`:

```js
const cache: Record<string, string> = {};
```

The first type argument to `Record` is for the key, and the second is for the value. In our case, both are strings.

The Record type allows us to add any number of dynamic keys to the object at runtime, using something like this:

```js
cache["keyHere"] = "valueHere";
```

Record is different than the Set and Map we looked at earlier– it is only at the type level.

## Use an Index Signature

Here's another way to update `cache` to please TypeScript:

Recall that in the "before" code we had lots of errors like `"You can't use that to index this"`.

These errors were saying that they couldn't tell what types the key was. Whenever you see an error about an index, it's usually about an object key!

For this fix, we'll add what is called an index signature to our `cache`:

```js
const cache: {
	[id: string]: string,
} = {};
```

Index signatures put the name of the index and its type (string or number) inside of square brackets.

So from the above, we can see that `id` is the index for `cache`.

If we were to set it to `[id: number]: string`, every time we called `cache.add()` we would have to pass a number as the first argument.

## Use an Interface with an Index Signature

We can also create an interface for `Cache` that contains the index signature:

```js
interface Cache {
	[id: string]: string;
}
```

Then inside of the `createCache` function we would type `cache` as `Cache`:

```js
const createCache = () => {
  const cache: Cache = {};
```

This would work with a `type` instead of `interface` as well.

## Which One to Use?

All of these solutions are fine-- there's no pros or cons between them.

I find that the `Record` syntax is a bit easier to look at and parse what it's doing. It also displays helpful information when you hover over it.

Assigning dynamic keys to an object is a common pattern in JavaScript, and these techniques allow you to do it.
