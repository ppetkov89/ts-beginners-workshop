# Pass a Type Argument to Restrict Potential Types

The solution here is pretty interesting.

We can pass in a type argument to the Set to tell it what type it should be:

```js
const guitarists = new Set<string>();
```

When the argument is in place, we can only add items of that specific type.

This also enables useful things like hovering over `guitarists.add()` and seeing that it expects a string.

## Digging Deeper

You can pass in type arguments as well as function arguments to certain functions.

In this case since Set is a class that we're instantiating, we can command-click or right-click and say "Go to Definition".

Double-clicking takes us to a file called `lib.es2015.collection.d.ts` which is where certain parts of JavaScript are typed out.

There's an interface for Set that starts like this:

```js
interface Set<T> {
```

That `T` represents the type argument.

Back in our solution code, if you erase the type argument and go back to just `Set()`, hovering over it will show you that it is typed as `Set<unknown>`.

Lots of JavaScript constructs and popular libraries use this.

## Another Example

Let's create a new Map and pass string as a type argument:

```js
const map = new Map<string>()
```

Now when we hover, we see the following:

```js
Map<any, any>
```

This is because Map accepts two type arguments: the first is the key, and the second is the value.

If we wanted to create a map where the keys and values were both strings, it would look like this:

```js
const map = new Map<string, string>()

map.set('someKey', 'someValue')
```

Changing `someKey` to a number would now give us an error.

This concept of passing type arguments to certain functions and constructors and classes is crucial for understanding TypeScript as a whole!
