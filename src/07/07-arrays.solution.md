# Two Ways to Represent Arrays

There are two solutions to this problem.

## Add Square Brackets

The first solution is to add a couple of square brackets to `Post`:

```js
// inside the User interface
posts: Post[];
```

Now if we create a `posts` consts of the type `Post[]`, we'll get autocomplete when we populate the `id` and `title` properties:

```js
const posts: Post[] = [
	{
		id: 1,
		title: "autocomplete works",
	},
];
```

## Use a Generic Type

The second solution is to use a generic type.

This syntax uses the name of what we want (`Array`, in this case) followed by angle brackets with the type that will be in the array:

```js
// inside the User interface
posts: Array<Post>
```

The generic types are built into TypeScript without us having to import anything.

We'll be seeing more of them later when we get into Promises, Maps, and other more advanced types.

## Which one should you use?

Both options resolve to the same thing– it's just two different ways of writing it.

I like `Post[]` for arrays, but I wanted to show the generic type syntax since it comes up later.
