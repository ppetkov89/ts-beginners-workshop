# Selectively Construct Types from Other Types

We've looked at inheriting and combining types.

But what if we want to be more selective?

## Challenge

Given this `User` interface, create a new object type that only includes the `firstName` and `lastName` properties:

```js
interface User {
	id: string;
	firstName: string;
	lastName: string;
}
```

Read through [the TypeScript Utility](https://www.typescriptlang.org/docs/handbook/utility-types.html) Type docs to see what you can find.
Hint: there are a few ways to solve this challenge - see what you can come up with!
