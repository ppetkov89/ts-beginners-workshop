# Two Techniques for Creating New Types Based on Others

There are a couple of solutions here that both make use of TypeScript's built-in helpers.

## Use Omit

The first solution is to `Omit`.

According to TypeScript, this constructs a type with the properties of `T` except for those in type `K`.

Here's what the syntax looks like:

```js
type MyType = Omit<User, "id">;
```

What we're saying here is create a type with everything that `User` has except for `id`.

## Use Pick

The second solution is to use `Pick`, which is the inverse of `Omit`:

```js
type MyType = Pick<User, "firstName" | "lastName">;
```

Here we're taking `User` and picking its `firstName` and `lastName` properties.

Note that `Pick` provides autocompletion while `Omit` does not.

This is because you are able to omit keys that aren't present on the original type, which means you end up with all of its properties. This is an advanced problem that we will tackle down the road.

For now, just know that both of `Omit` and `Pick` are globally available in TypeScript and are extremely useful tools.
