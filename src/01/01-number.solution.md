# Add type annotations to the function arguments

The solution is to add type annotations to the function's arguments.

In this case, both `a` and `b` should be typed as `number`:

```js
export const addTwoNumbers = (a: number, b: number) => {
	return a + b;
};
```

## Type Annotations are Critical to Understanding TypeScript

TypeScript relies on type annotations to understand the contracts the function must meet. Type annotations are critical to understanding TypeScript.

**Whenever you create a function, you must always specify the types of each argument!**

To help you remember this fundamental rule, I recommend turning on strict mode for all TypeScript projects.

When strict mode is enabled, you'll get the `implicitly has 'any' type` error whenever you leave type annotations out.

_But why can't TypeScript just look at the `+` and know that `a` and `b` are numbers?_

### Experiment with Different Types

Try setting the types of `a` and `b` to be `string`.

Running the test again, TypeScript would give you an error:

```
Argument of type 'number' is not assignable to parameter of type 'string'

```

Updating the test code to pass strings into `addTwoNumbers` ends up giving runtime errors.

Changing a type to a Boolean won't work because you can't add a boolean to a number.

A string can have a number appended to it with `+`, but the test would fail.
