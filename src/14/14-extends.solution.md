# Extend Interfaces From Other Interfaces

The first thing to do is create a new `Base` interface with the `id` property:

```js
interface Base {
	id: string;
}
```

With that in place, we can use the `extends` keyword with each of the other interfaces and remove the `id`:

```js
interface User extends Base {
	firstName: string;
	lastName: string;
}

interface Post extends Base {
	title: string;
	body: string;
}

interface Comment extends Base {
	comment: string;
}
```

Adding `extends Base` makes it so the types inherit the properties of the `Base` interface.

Note that if the `User`, `Post`, and `Comment` were defined using the type keyword, we would not be able to use `extends`:

```js
// This won't work!
type Base {
  id: string;
}

// Syntax errors
type Comment extends Base {
  comment: string;
}
```

This is because `extends` is a property of `interface` that `type` doesn't have.

Interfaces can extend from other interfaces. This allows us to change things in just one place, which is really useful.

It's even possible to extend multiple interfaces.

For example, we can have `Post` extend from `Base` and `User`:

```js
interface Post extends Base, User {
	title: string;
	body: string;
}
```

Then when creating a new `post` variable, we would have autocomplete on all of the properties required.

Interfaces can be composed together in a neater way than if we were just using types alone. This is particularly useful for situations where we have a complex data model that you want to express in TypeScript.
