# Update Return Type Annotations for Async Functions

There are several solutions here.

## Do What TypeScript Suggests

In the starting point of the code, hovering over the error message in VS Code pops up a message:

```js
The return type of an async function or method must be the global Promise<T> type. Did you mean to write 'Promise<LukeSkywalker>'?
```

We get this error because the `fetchLukeSkywalker` function is async, so the type returned will be a Promise.

Following TypeScript's suggestion to write `Promise<LukeSkywalker>` will fix this error:

```js
export const fetchLukeSkywalker = async (): Promise<LukeSkywalker> => {
```

Note that this syntax is similar to the `Array<Post>` syntax we saw earlier. But unlike arrays, there's only one way to use `Promise<T>`.

This solution works well, and is probably what I'd recommend.

## Type the Fetched Data Response

By default, the `data` returned from a fetch request will be typed as `Any`.

Even though we know from the API what our response will contain, TypeScript has no idea. That means that the `Any` type doesn't give us any autocomplete when we use it.

```js
export const fetchLukeSkywalker = async () => {
  // `data` will be typed as `Any`
  const data = await fetch(
```

But because we've awaited the fetch, we can type `data` as `LukeSkywalker`:

```js
const data: LukeSkywalker = await fetch(
```

This would give us the autocomplete because it's been properly typed.

Now when we hover over the `fetchLukeSkywalker` function declaration, we can see that TypeScript has inferred the return type of the function to be `Promise<LukeSkywalker>` just like we saw in the prior solution.

## Cast Data as a Type

In the cases we've looked at so far, we're kind of lying to ourselves a little bit.

TypeScript doesn't know what we're going to return, so we have to tell it. But really, we don't know what the result of our fetch is going to be.

This is where this solution comes in:

```js
export const fetchLukeSkywalker = async () => {
  const data = await fetch("<https://swapi.dev/api/people/1>").then((res) => {
    return res.json();
  });

  // cast the data to LukeSkywalker
  return data as LukeSkywalker;
};

```

The `return data as LukeSkywalker` line casts the fetch response to our `LukeSkywalker` type.

When working with fetch requests, you should either cast the return data as a type or assign a type to the data when the request is made.

## More About Casting

Casting allows us to let anyone become LukeSkywalker:

```js
const john = {} as LukeSkywalker
```

Compare the above to if we said `john` was assignable to `LukeSkywalker`:

```js
const john: LukeSkywalker = {};
```

This would give us errors because the object doesn't include the appropriate properties. Generally speaking, the assigning syntax is safer than the `as` syntax, and is probably what you should do in most cases.
