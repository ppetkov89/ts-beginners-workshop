# Typing Functions

Now we'll move on to typing functions.

We have an `addListener` function here:

```js
const addListener = (onFocusChange: unknown) => {
	window.addEventListener("focus", () => {
		onFocusChange(true);
	});

	window.addEventListener("blur", () => {
		onFocusChange(false);
	});
};
```

The type of `onFocusChange` that is passed in is currently `unknown`.

What we want it to be is a function that will take in a single boolean argument.

## Challenge

Your challenge is to visit [the TypeScript Docs](https://www.typescriptlang.org/docs/handbook/2/functions.html) and work out how to appropriately type the `onFocusChange` function.
Hint: there are a few ways to solve this challenge - see what you can come up with!
