# Specify the Type of a Function

The answer here is pretty interesting.

You can declare a function type using this syntax:

```js
(isFocused: boolean) => void
```

The `isFocused` argument is a boolean, and we're returning void.

This syntax can be used inline like this:

```js
const addListener = (onFocusChange: (isFocused: boolean) => void) => {
```

Or it can be extracted to its own type like this:

```js
type FocusListener = (isFocused: boolean) => void;

const addListener = (onFocusChange: FocusListener) => {
```

## The Structure of a Function Type

The basic structure starts with the arguments and the return type, both of which can be whatever you want.

You can use `undefined` for the return type, but then your function has to actually return `undefined`.

For times when your function has no return value, you should use `void`.

## Using Function types

Here's an example of adding an `isGreat` boolean as an additional argument:

```js
type FocusListener = (isFocused: boolean, isGreat: boolean) => void;
```

Then whenever we called the `FocusListener` we would have to include parameters for both:

```js
const addListener = (onFocusChange: FocusListener) => {
	onFocusChange(true, true);
};
```

We can also use function types in the same way we're used to using other types. Just be aware that your return types match appropriately.

```js
const myFocusListener = (isFocused: boolean): void => {
	// TypeScript error that `{}` is unassignable to void
	return {};
};
```

Most of the time you're not going to need these types unless you're passing functions to other functions, or declaring the type of the function.
