# Delineate Types a Value Can Be

The solution is to update `role` to be a Union type.

The syntax uses `|` to delineate the options for which values a key can be:

```js
interface User {
	id: number;
	firstName: string;
	lastName: string;
	role: "admin" | "user" | "super-admin";
}
```

With this change, removing the `// @ts-expect-error` line will show us the error message along with autocomplete for the allowed options.

## About Union Types

Anything can be added to a union type. For example, we can make a new `SuperAdmin` type and add it to the role:

```js
type SuperAdmin = "super-admin";

interface User {
	// ...other stuff
	role: "admin" | "user" | SuperAdmin;
}
```

In this example, an object primitive is added, and Prettier reformats the code like so:

```js

role:
  | "admin"
  | "user"
  | SuperAdmin
  | {
      wow: boolean;
    }
```

Union types are everywhere within TypeScript, and will be used throughout Total TypeScript.

They give your code safety and allow you to be really sure of the types of data flowing through your app.
