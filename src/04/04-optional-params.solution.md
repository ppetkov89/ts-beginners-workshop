# Mark a Function Parameter as Optional

Similar to last time, adding a `?` will mark `last` as optional:

```js
export const getName = (first: string, last?: string) => {
```

There is a caveats, however.

You can't put the optional argument `before` the required one:

```js
// this will not work! export const getName = (last?: string, first: string) => {
```

This would result in "Required parameter cannot follow an optional parameter".

As before, you could use `last: string | undefined` but you would then have to pass `undefined` as the second argument.
