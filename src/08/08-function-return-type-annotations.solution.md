# Annotate a Function to Specify its Return Type

The solution is to add a colon and the type you want to return after the parentheses when the function is defined.

In this case we'll add `: User` since we want to return a `User`.

Before:

```js
const makeUser = () => {
```

After:

```js
const makeUser = (): User => {
```

Specifying the type a function returns will check your code as you write it.

For example, `User` expects `id` to be a number. If you pass it a string, TypeScript will show you the error inside of the return object right away.

Notice that if you don't include everything that `User` expects, the errors all show within the `makeUser` function where the errors are instead of in the tests at the bottom of the file.

Adding type return annotations allows you to be more strict in ensuring that your function is safe on the inside as well as the outside.

## Experiment Without the Return Type

If you erase the `: User` from the `makeUser` function declaration, hovering over the function in VS Code will display a preview of what the function returns.

What's impressive about it is that it infers the types based on what's present in the return object. Even if you comment out the `User`, TypeScript will show information about each of the object's properties.
