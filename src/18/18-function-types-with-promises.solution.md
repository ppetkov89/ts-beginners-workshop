# Specify the Type for an Async Function

Here's the correct syntax for typing these functions:

```js
const createThenGetUser = async (
	createUser: () => Promise<string>,
	getUser: (id: string) => Promise<User>
): Promise<User> => {
	const userId: string = await createUser();

	const user = await getUser(userId);

	return user;
};
```

Same as before, these can be extracted out into their own types as well:

```js
type GetUser = (id: string) => Promise<User>;
```

What this is saying is that we have an `id` which is a string, and the function will return a Promise containing a `User`.

If we had `GetUser` only returning the `User` without the Promise, we won't be able to call it as an async function in the test due to errors.

Wrapping function return types with Promises is really useful when working with real world processes like fetching from databases.
