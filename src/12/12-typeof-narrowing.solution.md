# Change Function Returns Based on Type

For this solution, we'll start with getting the tests to pass and then work on the types.

Inside of the `coerceAmount` function we can check if `amount.amount` exists, then return it. Otherwise, return `amount`:

```js
const coerceAmount = (amount: number | { amount: number }) => {
	if (amount.amount) {
		return amount.amount;
	}
	return amount;
};
```

Now our tests will pass, but TypeScript gives us an error.

Hovering over `amount.amount` tells us:

`Property 'amount' does not exist on type number.`

This is a TypeScript quirk where you can't access a property unless you know it's there.

## Check Amount's Type

One way to fix the type error is updating our `if` to check if the `typeof` amount is an object. If so, return `amount.amount` otherwise just return `amount`.

```js
const coerceAmount = (amount: number | { amount: number }) => {
	if (typeof amount === "object") {
		return amount.amount;
	}
	return amount;
};
```

This would also work by checking if `typeof` amount is number, then return the amount. Otherwise, return `amount.amount`:

```js
const coerceAmount = (amount: number | { amount: number }) => {
	if (typeof amount === "number") {
		return amount;
	}
	return amount.amount;
};
```

Using the `typeof` operator to narrow down different branches of a union type is really powerful technique.

It's also really useful when interfacing with APIs that you didn't create.
