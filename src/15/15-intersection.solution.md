# Combine Inline or at the Type Level

This syntax might look confusing, but it is exactly what we want:

```js
export const getDefaultUserAndPosts = (): User & { posts: Post[] } => {
```

It says that we want to return a `User` as well as an array of `Posts`.

The `&` tells TypeScript that we want to `intersect` the two.

This is similar to what we saw with `extends` previously, but this time instead of inheriting, we are combining.

With `extends` you inherit, and with `&` you combine.

So we are making a single return type by combining `User` with an array of `Posts`.

It's possible to intersect multiple types, including those we create inline.

For example, we can also add an `age`:

```js
export const getDefaultUserAndPosts = (): User & { posts: Post[] } & { age: number} => {
```

If the return type starts to get too messy, you can do the intersection at the type level:

```js
type DefaultUserAndPosts = (): User & { posts: Post[] } & { age: number}

export const getDefaultUserAndPosts = (): DefaultUserAndPosts => {
```

Now when working with the `DefaultUserAndPosts` type, we would get autocompletion on all of the required properties.

Intersection is usually used for composing types from other types, but can be useful in other situations as well.
