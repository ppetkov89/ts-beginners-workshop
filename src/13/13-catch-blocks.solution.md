# Techniques for Coercing an Unknown Type

As mentioned, there are several ways to solve this problem.

## Use Any Type

One way to solve this challenge is to type `e` in the `catch` as `any`:

```js
} catch (e: any) {
  return e.message;
}
```

We can use `any` because we're pretty confident that the error is going to be an error.

What makes this solution not ideal is that we lose autocomplete on the `e`. This makes it easy to end up with typos that can cause issues later.

This isn't the solution I would suggest.

## Coerce as Error

A slightly better solution would be to coerce `e` to be an `Error`, and return the message:

```js
} catch (e) {
  return (e as Error).message;
}
```

Here `e` is still `unknown` when we enter the `catch`, but using `as` coerces it to an `Error` and we get our autocomplete as expected.

This solution is about as unsafe as the one above - we're not `checking` if `e` is an Error, we're casting it as an error.

## Check with instanceof

This solution is pretty similar to the last, except this time we'll use `instanceof` to see if `e` is an `Error`:

```js
} catch (e) {
  if (e instanceof Error) {
    return e.message;
  }
}
```

Unlike previous solutions, this time we're checking `e` at runtime to whittle down what it could be.

We're not blanketing it with the `any` type and we're not casting it to something else either.

This is my recommended solution, because it's the safest.

If you get something that isn't an error, it will fall down to the next block where you could do something else with it.

However, depending on the constraints of your codebase and how much you care about avoiding `any`s, you may prefer one of the other solutions.
